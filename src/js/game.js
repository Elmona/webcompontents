'use strict'

import cssTemplate from './css.js'
import htmlTemplate from './html.js'
import radioButtons from './radioButton.js'

export default class Game extends window.HTMLElement {
  constructor() {
    super()
    this.attachShadow({ mode: 'open' })

    this.shadowRoot.appendChild(cssTemplate.content.cloneNode(true))
    this.shadowRoot.appendChild(htmlTemplate.content.cloneNode(true))

    this.addBtnEventListener()
  }

  addBtnEventListener() {
    const btn = this.getElementFromShadowRoot('#start')

    btn.addEventListener('click', () => {
      const data = {
        id: 21,
        question: 'What is 2 + 8?',
        alternatives: { alt1: 2, alt2: 8, alt3: 10, alt4: 28 },
        nextURL: '127.0.0.1',
        message:
          'You got your question! Now send me which alternative that is right (the key) as the answer via HTTP POST to the nextURL in JSON-format'
      }

      const buttons = radioButtons(data.alternatives)
      this.appendToShadowDom('#radioButtons', buttons)
    })
  }

  appendToShadowDom(selector, element) {
    this.shadowRoot.querySelector(selector)
      .appendChild(element.content.cloneNode(true))
  }

  getElementFromShadowRoot(selector) {
    return this.shadowRoot.querySelector(selector)
  }
}
