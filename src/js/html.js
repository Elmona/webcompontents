'use strict'

const template = document.createElement('template')

template.innerHTML = /* html */`
<div>
  <p>Do you want to play a game?</p>
  <button id="start">Play the game!</button><br>
  <div id="radioButtons"></div>
</div>
`

export default template
