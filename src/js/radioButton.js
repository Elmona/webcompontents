'use strict'

const radioButton = alternatives => {
  const template = document.createElement('template')

  template.innerHTML = /* html */`
  <form action="">
  ${buttons(alternatives)}
  </form>
  `
  return template
}

const buttons = alternatives =>
  Object.keys(alternatives)
    .map(x => `<input type="radio" name="r" id="${x}" value="${x}"><label for="${x}">${alternatives[x]}</label><br>`)
    .join('')

export default radioButton
