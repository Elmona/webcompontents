'use strict'

const template = document.createElement('template')

template.innerHTML = /* html */`
<style>
  :host {
    display: flex;
    align-items: center;
    justify-content: center;
    background: #CCCCCC;
    border: 2px solid;
    width: 400px;
    height: 400px;
  }
</style>
`

export default template